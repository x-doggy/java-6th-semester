package com.stadnik.javasixthsemester;

import com.stadnik.javasixthsemester.Rectangle.Rectangle;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.*;

public class RectangleTest {
    private static final File rectData = new File("rect.dat");

    @Test
    public void writeToStreamTest() throws IOException {
        Rectangle rect = new Rectangle(3, 15, 1, 5);

        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(rectData))) {
            out.writeInt(rect.getTop());
            out.writeInt(rect.getLeft());
            out.writeInt(rect.getBottom());
            out.writeInt(rect.getRight());
        }

        assertEquals(rectData.length(), 16);
    }

    @Test
    public void readFromStreamTest() throws IOException {
        final int top, left, bottom, right;

        try (DataInputStream in = new DataInputStream(new FileInputStream(rectData))) {
            top = in.readInt();
            left = in.readInt();
            bottom = in.readInt();
            right = in.readInt();
        }

        Rectangle rect = new Rectangle(top, left, bottom, right);

        assertEquals(rect.getTop(), 3);
        assertEquals(rect.getLeft(), 15);
        assertEquals(rect.getBottom(), 1);
        assertEquals(rect.getRight(), 5);
    }

    @Test
    public void reverseTest() throws IOException {
        Rectangle[] rects = new Rectangle[5];
        rects[0] = new Rectangle(1, 1, 15, 10);
        rects[1] = new Rectangle(3, 2, 13, 9);
        rects[2] = new Rectangle(4, 3, 12, 8);
        rects[3] = new Rectangle(5, 4, 10, 7);
        rects[4] = new Rectangle(6, 5, 9, 6);

        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(rectData))) {
            // write to file
            for (Rectangle i : rects) {
                out.writeInt(i.getTop());
                out.writeInt(i.getLeft());
                out.writeInt(i.getBottom());
                out.writeInt(i.getRight());
            }
        }

        assertEquals(rectData.length(), 80); // sizeof(int) * 4 * 5

        // read from file
        try (RandomAccessFile raf = new RandomAccessFile(rectData, "r")) {
            int offset = 64;
            for (Rectangle i : rects) {
                raf.seek(offset);
                i.setTop(raf.readInt());
                i.setLeft(raf.readInt());
                i.setBottom(raf.readInt());
                i.setRight(raf.readInt());
                offset -= 16;
            }
        }

        assertEquals(rects[0], new Rectangle(6,5,9,6));
        assertEquals(rects[1], new Rectangle(5, 4, 10, 7));
        assertEquals(rects[2], new Rectangle(4, 3, 12, 8));
        assertEquals(rects[3], new Rectangle(3, 2, 13, 9));
        assertEquals(rects[4], new Rectangle(1, 1, 15, 10));
    }

    @Test
    public void rectPrintStreamTest() throws IOException {
        Rectangle rect = new Rectangle(1, 10, 20, 5);
        File textRect = new File("rect.txt");

        try (PrintStream stream = new PrintStream(textRect)) {
            stream.println(rect);
        }

        assertEquals(textRect.length(), 47);
    }
}
