package com.stadnik.javasixthsemester;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.stadnik.javasixthsemester.Color.Color;
import com.stadnik.javasixthsemester.Group.Group;
import com.stadnik.javasixthsemester.Group.GroupException;
import com.stadnik.javasixthsemester.Institute.Institute;
import com.stadnik.javasixthsemester.Institute.InstituteException;
import com.stadnik.javasixthsemester.Trainee.Trainee;
import com.stadnik.javasixthsemester.Trainee.TraineeException;

public class CollectionsTest {

  private final Trainee[] trainees;
  private final Group group;
  private final int LAST;

  public CollectionsTest() throws GroupException, TraineeException {
    this.trainees = new Trainee[]{
      new Trainee("John", "Doe", 1),
      new Trainee("Snoop", "Dogg", 2),
      new Trainee("Warren", "G", 3),
      new Trainee("Steve", "Wozniak", 4),
      new Trainee("Steve", "Jobs", 3),
      new Trainee("Bill", "Gates", 2),
      new Trainee("Linus", "Torvalds", 5),};
    this.group = new Group("МПБ-403-О", trainees);
    this.LAST = this.trainees.length - 1;
  }

  @Test(expected = TraineeException.class)
  public void TraineeCreationTest() throws TraineeException {
    new Trainee("", "", -1);
  }

  @Test(expected = GroupException.class)
  public void GroupCreationTest() throws GroupException {
    new Group("МПБ-403-О", null);
  }

  @Test
  public void groupSortTest() throws GroupException, TraineeException {
    Arrays.sort(group.getTrainees(), Comparator.comparing(Trainee::getValue));
    assertEquals(group.getTrainees()[0].getName(), "John");
    assertEquals(group.getTrainees()[LAST].getName(), "Linus");

    Arrays.sort(group.getTrainees(), Comparator.comparing(Trainee::getName));
    assertEquals(group.getTrainees()[0].getName(), "Bill");
    assertEquals(group.getTrainees()[LAST].getName(), "Warren");
  }

  @Test
  public void searchTraineeByName() throws GroupException, TraineeException {
    final String nameToSearch = "Linus";
    List<String> traineeNames = new LinkedList<>();
    for (Trainee t : group.getTrainees()) {
      traineeNames.add(t.getName());
    }
    assertTrue(traineeNames.contains(nameToSearch));
  }

  @Test
  public void traineeArrayListTest() throws TraineeException {
    LinkedList<Trainee> traineesList = new LinkedList<>(Arrays.asList(this.trainees));

    Collections.reverse(traineesList);
    assertEquals(traineesList.get(0).getSurname(), "Torvalds");
    assertEquals(traineesList.get(LAST).getSurname(), "Doe");
    Collections.reverse(traineesList); // return to normal order

    Collections.rotate(traineesList, 2);
    assertEquals(traineesList.get(0).getSurname(), "Gates");
    assertEquals(traineesList.get(LAST).getSurname(), "Jobs");

    Collections.shuffle(traineesList);

    Trainee hasMaxValue = Collections.max(traineesList, Comparator.comparing(Trainee::getValue));
    assertEquals(hasMaxValue.getSurname(), "Torvalds");

    Collections.sort(traineesList, Comparator.comparing(Trainee::getValue));
    assertEquals(traineesList.get(0).getSurname(), "Doe");
    assertEquals(traineesList.get(LAST).getSurname(), "Torvalds");

    Collections.sort(traineesList, Comparator.comparing(Trainee::getName));
    assertEquals(traineesList.get(0).getSurname(), "Gates");
    assertEquals(traineesList.get(LAST).getSurname(), "G");

    List<String> traineeNames = new LinkedList<>();
    for (Trainee t : group.getTrainees()) {
      traineeNames.add(t.getName());
    }
    assertTrue(traineeNames.contains("Linus"));
  }

  @Test
  public void arrayListVSLinkedListTest() {
    List<Integer> arrayList = new ArrayList<>();
    List<Integer> linkedList = new LinkedList<>();
    long diffTime1, diffTime2;
    final int valueToGet = 87359;

    for (int i = 0; i < 99999; i++) {
      arrayList.add(i);
      linkedList.add(i);
    }

    Function<List<Integer>, Long> mesaure = c -> {
      long start, finish;
      start = System.nanoTime();
      c.get(valueToGet);
      finish = System.nanoTime();
      return finish - start;
    };

    diffTime1 = mesaure.apply(arrayList);
    System.out.println("ArrayList: " + diffTime1 + " nanoseconds");

    diffTime2 = mesaure.apply(linkedList);
    System.out.println("LinkedList: " + diffTime2 + " nanoseconds");

    assertTrue(diffTime2 > diffTime1);

    // Result: linked list goes 7 times longer!
  }

  @Test
  public void queueTest() throws TraineeException {
    Queue<Trainee> traineeQueue = new PriorityQueue<>(Arrays.asList(trainees));
    for (Trainee t : traineeQueue) {
      System.out.println(t);
    }
    assertNotNull(traineeQueue.peek());
  }

  @Test
  public void hashSetTest() throws TraineeException {
    HashSet<Trainee> traineeHashSet = new HashSet<>(Arrays.asList(trainees));
    assertTrue(traineeHashSet.contains(new Trainee("Steve", "Jobs", 3)));
  }

  @Test
  public void treeSetTest() throws TraineeException {
    TreeSet<Trainee> traineeTreeSet = new TreeSet<>(Arrays.asList(trainees));
    assertTrue(traineeTreeSet.contains(new Trainee("Steve", "Wozniak", 4)));
  }

  @Test
  public void treeSetSortedByName() throws TraineeException {
    Arrays.sort(trainees, Comparator.comparing(Trainee::getName));
    TreeSet<Trainee> traineeTreeSet = new TreeSet<>(Arrays.asList(trainees));
    traineeTreeSet.add(new Trainee("Nate", "Dogg", 4));
    assertTrue(traineeTreeSet.contains(new Trainee("Snoop", "Dogg", 2)));
  }

  @Test
  public void arrayVSListTest() throws TraineeException {
    Random random = new Random();

    List<Integer> arrayList = new ArrayList<>();
    HashSet<Integer> hashSet = new HashSet<>();
    TreeSet<Integer> treeSet = new TreeSet<>();

    long diff1, diff2, diff3;
    final int tooManyTimes = 10000;

    for (int i = 0; i < tooManyTimes; i++) {
      final int value = random.nextInt();
      arrayList.add(value);
      hashSet.add(value);
      treeSet.add(value);
    }
    final int valueToSearch = random.nextInt();

    Function<Collection<Integer>, Long> mesaure = c -> {
      long start, finish;
      start = System.nanoTime();
      for (int i = 0; i < tooManyTimes; i++) {
        c.contains(valueToSearch);
      }
      finish = System.nanoTime();
      return finish - start;
    };

    diff1 = mesaure.apply(arrayList);
    diff2 = mesaure.apply(hashSet);
    diff3 = mesaure.apply(treeSet);

    System.out.println(diff1);
    System.out.println(diff2);
    System.out.println(diff3);

    assertTrue(diff2 < diff3 && diff2 < diff1 && diff3 < diff1);
  }

  private static Trainee getTraineeByName(final Trainee[] trs, final String name) {
    for (Trainee t : trs) {
      if (t.getName().equals(name)) {
        return t;
      }
    }
    return null;
  }

  private static Trainee getTraineeBySurname(final Trainee[] trs, final String surname) {
    for (Trainee t : trs) {
      if (t.getSurname().equals(surname)) {
        return t;
      }
    }
    return null;
  }

  @Test
  public void hashMapTest() throws TraineeException, InstituteException {
    final Trainee snoop = getTraineeByName(trainees, "Snoop");
    final Trainee linus = getTraineeByName(trainees, "Linus");
    final Trainee warren = getTraineeByName(trainees, "Warren");

    Institute lbcPolytechnicHighSchool = new Institute("Long Beach Polytechnic High School", "Long Beach, California");
    Institute helsinki = new Institute("University of Helsinki", "Helsinki, Finland");

    HashMap<Trainee, Institute> hashMap = new HashMap<>();
    hashMap.put(snoop, lbcPolytechnicHighSchool);
    hashMap.put(warren, lbcPolytechnicHighSchool);
    hashMap.put(linus, helsinki);

    // функциональненько...
    Set<Institute> result = hashMap
            .entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getKey(), warren))
            .map(Map.Entry::getValue)
            .collect(Collectors.toSet());

    assertEquals(result.toArray()[0], lbcPolytechnicHighSchool);

    for (Trainee t : hashMap.keySet()) {
      System.out.println(t);
    }
  }

  @Test
  public void treeMapTest() throws TraineeException, InstituteException {
    final Trainee wozniak = getTraineeBySurname(trainees, "Wozniak");
    final Trainee jobs = getTraineeBySurname(trainees, "Jobs");
    final Trainee gates = getTraineeBySurname(trainees, "Gates");

    Institute reed = new Institute("Reed college", "Portland, US");
    Institute harvard = new Institute("Harvard", "Cambridge, Massachusetts");

    TreeMap<Trainee, Institute> treeMap = new TreeMap<>();
    treeMap.put(wozniak, reed);
    treeMap.put(jobs, reed);
    treeMap.put(gates, harvard);

    Set<Institute> result = treeMap
            .entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getKey(), wozniak))
            .map(Map.Entry::getValue)
            .collect(Collectors.toSet());

    assertEquals(result.toArray()[0], reed);

    for (Map.Entry<Trainee, Institute> e : treeMap.entrySet()) {
      System.out.println(e.getKey() + " => " + e.getValue());
    }

    for (Trainee t : treeMap.keySet()) {
      System.out.println(t);
    }
  }

  @Test
  public void bitSetTest() {
    BitSet bs = new BitSet(32);
    bs.set(1);
    bs.set(5);
    bs.set(10);
    bs.set(17);
    bs.set(31);

    assertTrue(bs.get(5));
    bs.clear(5, 17);
    assertFalse(bs.get(5));
  }

  @Test
  public void setConcurrenceTest() throws TraineeException {
    final int SIZE = 1000000;
    BitSet bs = new BitSet(SIZE);
    HashSet<Integer> hashSet = new HashSet<>();
    TreeSet<Integer> treeSet = new TreeSet<>();
    long start, finish, diff1, diff2, diff3;

    // Check a bit set
    start = System.nanoTime();
    for (int i = 0; i < SIZE; i++) {
      bs.set(i);
    }
    finish = System.nanoTime();
    diff1 = finish - start;
    System.out.println("Bit set: " + diff1);

    // Check a hash set
    start = System.nanoTime();
    for (int i = 0; i < SIZE; i++) {
      hashSet.add(i);
    }
    finish = System.nanoTime();
    diff2 = finish - start;
    System.out.println("Hash set: " + diff2);

    // Check a tree set
    start = System.nanoTime();
    for (int i = 0; i < SIZE; i++) {
      treeSet.add(i);
    }
    finish = System.nanoTime();
    diff3 = finish - start;
    System.out.println("Tree set: " + diff3);
  }

  @Test
  public void enumSetTest() {
    EnumSet<Color> enumAll = EnumSet.allOf(Color.class);
    EnumSet<Color> enumBlue = EnumSet.of(Color.BLUE);
    EnumSet<Color> enumRange = EnumSet.range(Color.RED, Color.BLUE);
    EnumSet<Color> enumEmpty = EnumSet.noneOf(Color.class);

    assertTrue(enumAll.contains(Color.BLACK));
    assertTrue(enumBlue.contains(Color.BLUE));
    assertFalse(enumBlue.contains(Color.WHITE));
    assertTrue(enumRange.contains(Color.GREEN));
    assertFalse(enumRange.contains(Color.WHITE));
    assertFalse(enumEmpty.contains(Color.RED));
  }

  private static boolean isSimilarRows(final int[][] matrix, int m, int n) {
    Set<Integer> set1 = new TreeSet<>();
    Set<Integer> set2 = new TreeSet<>();

    for (int j = 0; j < matrix.length; j++) {
      set1.add(matrix[m][j]);
      set2.add(matrix[n][j]);
    }

    return set1.equals(set2);
  }

  @Test
  public void similarRowsInArrayTest() {
    int[][] matrix = {
      {1, 2, 3},
      {3, 2, 1},
      {4, 2, 1}
    };

    assertTrue(isSimilarRows(matrix, 0, 1));
    assertFalse(isSimilarRows(matrix, 0, 2));
    assertFalse(isSimilarRows(matrix, 1, 2));
  }
}
