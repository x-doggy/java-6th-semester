package com.stadnik.javasixthsemester;

import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.nio.channels.*;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import static org.junit.Assert.*;

public class NioTest {

    @Test
    public void zeroToNinetyNineTest() throws IOException {
        final int SIZE = 99;

        try (FileChannel fc = new RandomAccessFile(new File("0-99.txt"), "rw").getChannel()) {
            MappedByteBuffer buf = fc.map(FileChannel.MapMode.READ_WRITE, 0, SIZE * 4);
            for (int i = 0; i < SIZE; i++) {
                buf.putInt(i);
            }
            /* wrote, now check this out */
            buf.position(0);
            for (int i = 0; i < SIZE; i++) {
                assertEquals(buf.getInt(), i);
            }
        }
    }

    @Test
    public void pathTest() throws IOException {
        Path path = Paths.get("course3.iml");
        assertFalse(path.isAbsolute());
        assertEquals(path.getFileName().toString(), "course3.iml");

        Path path2 = Paths.get("tmp/my/java.txt");
        assertFalse(path2.isAbsolute());
        assertEquals(path2.getName(1).toString(), "my");
        assertTrue(path2.startsWith("tmp/my"));
    }

    @Test
    public void filesTest() throws IOException {
        Path file = Paths.get("file.txt");
        Path file2 = Paths.get("file2.txt");
        Path symLink = Paths.get("sym.txt");

        /* Prepare for tests */
        Files.deleteIfExists(file);
        Files.deleteIfExists(file2);
        Files.deleteIfExists(symLink);

        Files.createFile(file);
        assertTrue(Files.exists(file));

        Files.copy(file, file2, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
        assertTrue(Files.exists(file2));

        Files.delete(file);
        assertFalse(Files.exists(file));

        Files.createSymbolicLink(symLink, file2);
        assertTrue(Files.isSymbolicLink(symLink));
    }

    @Test
    public void renameDatToBin() throws IOException {
        Path gameData = Paths.get("gameData.dat");
        Path otherData = Paths.get("otherData.dat");
        Path myData = Paths.get("myData.dat");
        Path yourData = Paths.get("yourData.dat");

        Files.createFile(gameData);
        Files.createFile(otherData);
        Files.createFile(myData);
        Files.createFile(yourData);

        assertTrue(Files.exists(gameData));
        assertTrue(Files.exists(otherData));
        assertTrue(Files.exists(myData));
        assertTrue(Files.exists(yourData));

        String gameDataBin = FilenameUtils.getBaseName(gameData.toString()) + ".bin";
        String otherDataBin = FilenameUtils.getBaseName(otherData.toString()) + ".bin";
        String myDataBin = FilenameUtils.getBaseName(myData.toString()) + ".bin";
        String yourDataBin = FilenameUtils.getBaseName(yourData.toString()) + ".bin";

        assertEquals(gameDataBin, "gameData.bin");
        assertEquals(otherDataBin, "otherData.bin");
        assertEquals(myDataBin, "myData.bin");
        assertEquals(yourDataBin, "yourData.bin");

        gameData.toFile().renameTo(new File(gameDataBin));
        otherData.toFile().renameTo(new File(otherDataBin));
        myData.toFile().renameTo(new File(myDataBin));
        yourData.toFile().renameTo(new File(yourDataBin));

        assertEquals(FilenameUtils.getName(gameDataBin), "gameData.bin");
        assertEquals(FilenameUtils.getName(otherDataBin), "otherData.bin");
        assertEquals(FilenameUtils.getName(myDataBin), "myData.bin");
        assertEquals(FilenameUtils.getName(yourDataBin), "yourData.bin");

        Files.deleteIfExists(Paths.get(gameDataBin));
        Files.deleteIfExists(Paths.get(otherDataBin));
        Files.deleteIfExists(Paths.get(myDataBin));
        Files.deleteIfExists(Paths.get(yourDataBin));
    }
}
