package com.stadnik.javasixthsemester;

import com.stadnik.javasixthsemester.Trainee.TraineeException;
import com.stadnik.javasixthsemester.Trainee.Trainee;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;

import org.junit.Test;
import static org.junit.Assert.*;
import com.google.gson.Gson;

public class TraineeTest {

    /*  +--------------------------------------------------------+
     *  |  Simple tests such as constructor, get and set methods |
     *  +--------------------------------------------------------+
     */

    @Test(expected = TraineeException.class)
    public void constructorTest() throws TraineeException {
        new Trainee("John", "Doe", -4);
    }

    @Test
    public void getTest() throws TraineeException {
        Trainee tr = new Trainee("John", "Doe", 4);
        assertEquals(tr.getName(), "John");
        assertEquals(tr.getSurname(), "Doe");
        assertEquals(tr.getValue(), 4);
    }

    @Test(expected = TraineeException.class)
    public void setTest() throws TraineeException {
        Trainee tr = new Trainee("John", "Doe", 2);
        tr.setName("John");
        tr.setSurname("");
    }



    /*  +----------------------------------------+
     *  |  Write and read trainee with line feed |
     *  +----------------------------------------+
     */

    private static void writeLnTrainee(Trainee tr, File traineeTxt) throws FileNotFoundException {
        try (PrintStream stream = new PrintStream(traineeTxt)) {
            stream.println(tr.getName());
            stream.println(tr.getSurname());
            stream.println(tr.getValue());
        }
    }

    private static Trainee readLnTrainee(File traineeTxt) throws TraineeException, IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(traineeTxt))) {
            return new Trainee(br.readLine(), br.readLine(), Integer.valueOf(br.readLine()));
        }
    }

    @Test
    public void writeAndReadLnTraineeTest() throws TraineeException, IOException {
        Trainee tr1 = new Trainee("John", "Doe", 4);
        File trText = new File("trainee1.txt");
        System.setProperty("line.separator", "\r\n");

        // Writeln Trainee
        writeLnTrainee(tr1, trText);
        assertEquals(trText.length(), 14);

        // Readln Trainee
        Trainee tr2 = readLnTrainee(trText);
        assertEquals(tr1, tr2);
    }



    /*  +-------------------------------------------+
     *  |  Write and read trainee WITHOUT line feed |
     *  +-------------------------------------------+
     */

    private static void writeTrainee(Trainee tr, File traineeTxt) throws IOException {
        try (PrintStream stream = new PrintStream(traineeTxt)) {
            stream.print(tr.getName() + " ");
            stream.print(tr.getSurname() + " ");
            stream.print(tr.getValue());
        }
    }

    private static Trainee readTrainee(File traineeTxt) throws TraineeException, IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(traineeTxt))) {
            final String[] line = br.readLine().split(" ");
            final String name = line[0];
            final String surname = line[1];
            final Integer value = Integer.valueOf(line[2]);

            return new Trainee(name, surname, value);
        }
    }

    @Test
    public void writeAndReadTraineeTest() throws TraineeException, IOException {
        Trainee tr1 = new Trainee("Foo", "Bar", 5);
        File trText = new File("trainee2.txt");

        // Write Trainee
        writeTrainee(tr1, trText);
        assertEquals(trText.length(), 9);

        // Read Trainee
        Trainee tr2 = readTrainee(trText);
        assertEquals(tr1, tr2);
    }



    /*  +-------------------------------------------+
     *  |  Some of read / write serialization tests |
     *  +-------------------------------------------+
     */

    @Test
    public void simpleSerializationTest() throws TraineeException, IOException, ClassNotFoundException {
        final File ser = new File("trainee.dat");
        Trainee tr1 = new Trainee("Foo", "Bar", 4);
        Trainee tr2;

        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ser))) {
            out.writeObject(tr1);
        }

        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(ser))) {
            tr2 = (Trainee) in.readObject();
        }

        assertEquals(tr1, tr2);
    }

    @Test
    public void byteArraySerializationTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee tr1 = new Trainee("John", "Doe", 5);
        Trainee tr2;
        byte[] trBytes;

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(bos)) {

            out.writeObject(tr1);
            trBytes = bos.toByteArray();
        }

        try (ByteArrayInputStream bis = new ByteArrayInputStream(trBytes);
             ObjectInputStream in = new ObjectInputStream(bis)) {

            tr2 = (Trainee) in.readObject();
        }

        assertEquals(tr1, tr2);
    }

    @Test
    public void byteBufferSerializationTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee tr1 = new Trainee("Foo", "Bar", 4);
        Trainee tr2;
        byte[] trBytes;

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(bos)) {

            out.writeObject(tr1);
            trBytes = bos.toByteArray();
        }

        ByteBuffer buf = ByteBuffer.allocate( trBytes.length );
        buf.put(trBytes);

        byte[] trAnotherBytes = new byte[ buf.capacity() ];
        ((ByteBuffer) buf.duplicate().clear()).get(trAnotherBytes);

        try (ByteArrayInputStream bis = new ByteArrayInputStream(trAnotherBytes);
             ObjectInputStream in = new ObjectInputStream(bis)) {

            tr2 = (Trainee) in.readObject();
        }

        /* Now see what we have... */
        assertEquals(tr1, tr2);
    }

    @Test
    public void jsonSerializationtest() throws TraineeException, IOException {
        final File traineeJson = new File("linus.json");
        Trainee tr1 = new Trainee("Linus", "Torvalds", 5);
        Trainee tr2;
        Gson gson = new Gson();

        try (FileWriter writer = new FileWriter(traineeJson)) {
            gson.toJson(tr1, writer);
        }

        try (FileReader reader = new FileReader(traineeJson)) {
            tr2 = gson.fromJson(reader, Trainee.class);
        }

        assertEquals(tr1, tr2);
    }

    @Test
    public void jsonPrintSerializationTest() throws TraineeException, IOException {
        final File trJsonFile = new File("bill.json");
        final File jsonText = new File("json.txt");

        Trainee tr1 = new Trainee("Bill", "Gates", 2);
        Trainee tr2;

        Gson gson = new Gson();

        try (FileWriter writer = new FileWriter(trJsonFile)) {
            gson.toJson(tr1, writer);
        }

        try (FileReader reader = new FileReader(trJsonFile);
             PrintStream stream = new PrintStream(jsonText)) {

            tr2 = gson.fromJson(reader, Trainee.class);
            stream.println(tr2);
            assertTrue(jsonText.length() > 0);
        }

        assertEquals(tr1, tr2);
    }



    /*  +---------------------------+
     *  |  java.nio.* buffers tests |
     *  +---------------------------+
     */

    @Test
    public void byteBufferTest() throws TraineeException, IOException {
        File trText = new File("trainee2.txt");
        writeTrainee(new Trainee("Foo", "Bar", 5), trText);

        final int SIZE = (int) trText.length();
        final byte[] magicValues = {70, 111, 111, 32, 66, 97, 114, 32, 53, 0};
        ByteBuffer buf = ByteBuffer.allocate(SIZE);

        try (FileChannel channel = new FileInputStream(trText).getChannel()) {
            channel.read(buf);
            buf.position(0);
            for (int i=0; i < buf.capacity(); i++)
                assertEquals(buf.get(), magicValues[i]);
        }
    }

    @Test
    public void mappingBufferTest() throws TraineeException, IOException {
        File trText = new File("trainee2.txt");
        writeTrainee(new Trainee("Foo", "Bar", 5), trText);

        final int SIZE = (int) trText.length();
        final byte[] magicValues = {70, 111, 111, 32, 66, 97, 114, 32, 53};

        try (FileChannel channel = new FileInputStream(trText).getChannel()) {
            MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, SIZE);
            for (int i=0; i < SIZE; i++) {
                assertEquals(buf.get(), magicValues[i]);
            }
        }
    }
}