package com.stadnik.javasixthsemester;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;

public class FileTest {

  private final File licence = new File("file1.dat");
  private final File authors = new File("file2.dat");
  
  @Test
  public void fileTest() throws IOException {
    /* create file */
    assertFalse(licence.exists());
    assertTrue(licence.createNewFile());
    
    /* is this file */
    assertTrue(licence.isFile());
    assertFalse(licence.isDirectory());
    
    /* get full path */
    assertNotEquals(licence.getAbsolutePath(), "");
    
    /* rename file */
    assertTrue(licence.renameTo(authors));
    
    /* delete file */
    assertTrue(authors.delete());
    
  }

}
