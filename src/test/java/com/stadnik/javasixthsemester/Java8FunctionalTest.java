package com.stadnik.javasixthsemester;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Java8FunctionalTest {

    class Person {
        private String firstName;
        private String lastName;
        private Person mom, dad;

        Person(String firstName, String lastName) throws Exception {
            this.setFirstName(firstName);
            this.setLastName(lastName);
            this.mom = null;
            this.dad = null;
        }

        Person(String firstName, String lastName, Person mom, Person dad) throws Exception {
            this.setFirstName(firstName);
            this.setLastName(lastName);
            this.mom = mom;
            this.dad = dad;
        }

        Person getMom() {
            return mom;
        }
        Person getDad() {
            return dad;
        }

        private boolean stringIsIncorrect(String str) {
            return str == null || str.equals("");
        }

        private void checkFirstName(String firstName) throws Exception {
            if (stringIsIncorrect(firstName))
                throw new Exception("Incorrect first name");
        }

        private void checkLastName(String lastName) throws Exception {
            if (stringIsIncorrect(lastName))
                throw new Exception("Incorrect last name");
        }

        void setFirstName(String firstName) throws Exception {
            checkFirstName(firstName);
            this.firstName = firstName;
        }

        void setLastName(String lastName) throws Exception {
            checkLastName(lastName);
            this.lastName = lastName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return firstName.equals(person.firstName) && lastName.equals(person.lastName);
        }

        @Override
        public int hashCode() {
            int result = firstName.hashCode();
            result = 31 * result + lastName.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", mom=" + mom +
                    ", dad=" + dad +
                    '}';
        }
    }


    interface PersonFactory<P extends Person> {
        P create(String firstName, String lastName) throws Exception;
    }

    @FunctionalInterface
    interface MyFunction<T, K> {
        K apply(T arg);
        //K apply(T arg1, T arg2);
    }

    private double op(double d1, double d2, DoubleBinaryOperator f) {
        return f.applyAsDouble(d1, d2);
    }

    private IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    @Test
    public void splitAndCountTest() {
        Function<String, List<String>> split = s -> {
            List<String> list = new ArrayList<>();
            list.addAll(Arrays.asList(s.split(" ")));
            return list;
        };

        Function<List<?>, Integer> count = List::size;
        Function<String, Integer> splitAndCountAndThen = s -> split.andThen(count).apply(s);
        Function<String, Integer> splitAndCountCompose = s -> count.compose(split).apply(s);

        final String line = "Do anybody in the house remember? When it wasn’t no Snoop Dogg? (Hell yeah!)";
        assertTrue(14 == count.apply(split.apply(line)));
        assertTrue(14 == splitAndCountAndThen.apply(line));
        assertTrue(14 == splitAndCountCompose.apply(line));
    }

    @Test
    public void personTest() throws Exception {
        PersonFactory<Person> pf = Person::new;
        Person p = pf.create("Vladimir", "Stadnik");
        assertEquals(p, new Person("Vladimir", "Stadnik"));
    }

    @Test
    public void maxTest() {
        assertEquals(5.4, op(-1000,5.4, Math::max), 0.1);
        assertEquals(2.0, op(2,3, Math::min), 0.1);
    }

    @Test
    public void isEvenTest() {
        Predicate<Integer> isEven = a -> a % 2 == 0;
        assertTrue(isEven.test(4));
        assertFalse(isEven.test(3));
    }

    @Test
    public void areEqualTest() {
        BiPredicate<Integer, Integer> areEqual = Objects::equals;
        assertTrue(areEqual.test(4, 4));
        assertFalse(areEqual.test(8, -1));
        assertTrue(areEqual.test(0, 0));
    }

    @Test
    public void myFunctionTest() {
        MyFunction<String, Integer> length = String::length;
        assertTrue(10 == length.apply("Snoop Dogg"));
        // MyFunction<String, Boolean> areEquals = (s1, s2) -> s1[0] != s2[0];
        // получается "MyFunction is not a functional interface"
    }

    @Test
    public void motherFatherByPersonTest() throws Exception {
        Person mothersMomDad = new Person("Justin", "Taylor");

        Person mothersMom = new Person("Hannah", "Davis", new Person("Sarah", "Davis"), mothersMomDad);
        Person mothersDad = new Person("Bob", "Davis");

        Person mom = new Person("Shirley", "Smith", mothersMom, mothersDad);
        Person dad = new Person("Eich", "Smith");

        Person me = new Person("John", "Smith", mom, dad);

        if (me.getMom() != null &&
            me.getMom().getMom() != null &&
            me.getMom().getMom().getDad() != null) {

            assertEquals(me.getMom().getMom().getDad(), mothersMomDad);
        }
    }

    @Test
    public void motherFatherByOptionalTest() {

        class OptionalPerson {
            private Optional<String> me;
            private Optional<OptionalPerson> mom;
            private Optional<OptionalPerson> dad;

            OptionalPerson(final String me) {
                this.me = Optional.ofNullable(me);
                this.mom = Optional.empty();
                this.dad = Optional.empty();
            }

            OptionalPerson(String me, OptionalPerson mom, OptionalPerson dad) {
                this.me = Optional.ofNullable(me);
                this.mom = Optional.ofNullable(mom);
                this.dad = Optional.ofNullable(dad);
            }

            public Optional<String> getMe() {
                return me;
            }

            public Optional<OptionalPerson> getMom() {
                return mom;
            }

            public Optional<OptionalPerson> getDad() {
                return dad;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                OptionalPerson that = (OptionalPerson) o;

                return (getMe() != null ? getMe().equals(that.getMe()) : that.getMe() == null) &&
                        (getMom() != null ? getMom().equals(that.getMom()) : that.getMom() == null) &&
                        (getDad() != null ? getDad().equals(that.getDad()) : that.getDad() == null);
            }

            @Override
            public int hashCode() {
                int result = getMe() != null ? getMe().hashCode() : 0;
                result = 31 * result + (getMom() != null ? getMom().hashCode() : 0);
                result = 31 * result + (getDad() != null ? getDad().hashCode() : 0);
                return result;
            }

            @Override
            public String toString() {
                return "OptionalPerson{" +
                        "me=" + me +
                        ", mom=" + mom +
                        ", dad=" + dad +
                        '}';
            }
        }

        Function<OptionalPerson, Optional<OptionalPerson>> getMothersMotherFather = me -> Optional.of(me)
            .flatMap(OptionalPerson::getMom)
            .flatMap(OptionalPerson::getMom)
            .flatMap(OptionalPerson::getDad);

        OptionalPerson mothersMomDad = new OptionalPerson("Justin Taylor");

        OptionalPerson mothersMom = new OptionalPerson("Hannah Davis", new OptionalPerson("Sarah Davis"), mothersMomDad);
        OptionalPerson mothersDad = new OptionalPerson("Bob Davis");

        OptionalPerson mom = new OptionalPerson("Shirley Smith", mothersMom, mothersDad);
        OptionalPerson dad = new OptionalPerson("Eich Smith");

        OptionalPerson me = new OptionalPerson("John Smith", mom, dad);

        assertEquals(Optional.of(mothersMomDad), getMothersMotherFather.apply(me));
    }

    @Test
    public void transformIntStream() {
        assertTrue(
            Arrays.equals(
                transform(
                    IntStream.rangeClosed(1, 5), x -> x * x).toArray(),
                    new int[] { 1, 4, 9, 16, 25 }
                )
        );
    }


    @Test
    public void ListPersonTest() {
        class OtherPerson {
            private final String name;
            private final int age;

            OtherPerson(String name, int age) {
                this.name = name;
                this.age = age;
            }

            String getName() {
                return name;
            }
            int getAge() {
                return age;
            }

            @Override
            public String toString() {
                return "OtherPerson{" +
                        "name='" + name + '\'' +
                        ", age=" + age +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                OtherPerson person = (OtherPerson) o;

                return getAge() == person.getAge() && getName().equals(person.getName());
            }

            @Override
            public int hashCode() {
                int result = getName().hashCode();
                result = 31 * result + getAge();
                return result;
            }
        }

        final Predicate<OtherPerson> ageCondition = person -> person.getAge() >= 30;

        List<OtherPerson> list = new ArrayList<>();
        list.add(new OtherPerson("Vladimir", 22));
        list.add(new OtherPerson("Dima", 21));
        list.add(new OtherPerson("Turovets", 20));
        list.add(new OtherPerson("Prolubnikov", 38));
        list.add(new OtherPerson("Carter", 34));
        list.add(new OtherPerson("Chen", 62));
        list.add(new OtherPerson("Dogg", 40));
        list.add(new OtherPerson("Dogg", 45));

        list.stream()
                .filter(ageCondition)
                .map(OtherPerson::getName)
                .distinct()
                .sorted(Comparator.comparingInt(String::length))
                .forEach(System.out::println);

        System.out.println("\n\n");

        list.stream()
                .filter(ageCondition)
                .map(OtherPerson::getName)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .forEach(System.out::println);
    }

    @Test
    public void reduceTest() {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= 7; i++)
            list.add(i);

        Optional<Integer> sum = list.stream().reduce((a, b) -> a + b);
        assertEquals(Optional.of(28), sum);

        Optional<Integer> product = list.stream().reduce((a, b) -> a * b);
        assertEquals(Optional.of(5040), product);
    }
}
