package com.stadnik.javasixthsemester;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class DirsTest {

  private final File dir = new File("dir");
  private final File dir2 = new File("dir2");
  
  @Test
  public void dirsTest() {
    dir.delete();
    dir2.delete();
    
    /* create dir */
    assertFalse(dir.exists());
    assertTrue(dir.mkdir());
    
    /* is this dir */
    assertTrue(dir.isDirectory());
    assertFalse(dir.isFile());

    /* get full path */
    assertNotEquals(dir.getAbsolutePath(), "");
    
    /* list dir files */
    assertNotNull(dir.list());
    
    try {
      new File("dir/Main.java").createNewFile();
      new File("dir/Test.java").createNewFile();
      new File("dir/main.c").createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }

    /* list files by masks */
    File[] filesByMask;
    filesByMask = dir.listFiles(f -> f.getName().endsWith(".java"));
    assertNotNull(filesByMask);
    Arrays.sort(filesByMask);

    filesByMask = dir.listFiles(f -> f.getName().equals("main.c"));
    assertNotNull(filesByMask);
    assertEquals(filesByMask[0].getName(), "main.c");
    
    /* rename file */
    assertTrue(dir.renameTo(dir2));
  }

}
