package com.stadnik.javasixthsemester.Trainee;

import java.io.Serializable;

public final class Trainee implements Serializable, Comparable<Trainee> {

  private static final long serialVersionUID = 55837821462938405L;

  private String name;
  private String surname;
  private int value;

  public Trainee(String name, String surname, int value) throws TraineeException {
    setName(name);
    setSurname(surname);
    setValue(value);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) throws TraineeException {
    checkName(name);
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) throws TraineeException {
    checkSurname(surname);
    this.surname = surname;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) throws TraineeException {
    checkValue(value);
    this.value = value;
  }

  private static void checkName(String name) throws TraineeException {
    if (name == null || name.equals("")) {
      throw new TraineeException(TraineeErrorCodes.WRONG_NAME, name);
    }
  }

  private static void checkSurname(String surname) throws TraineeException {
    if (surname == null || surname.equals("")) {
      throw new TraineeException(TraineeErrorCodes.WRONG_SURNAME, surname);
    }
  }

  private static void checkValue(int value) throws TraineeException {
    if (value < 1 || value > 5) {
      throw new TraineeException(TraineeErrorCodes.WRONG_VALUE, String.valueOf(value));
    }
  }

  @Override
  public String toString() {
    return "Trainee{"
            + "name='" + name + '\''
            + ", surname='" + surname + '\''
            + ", value=" + value
            + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Trainee)) {
      return false;
    }

    Trainee trainee = (Trainee) o;

    if (getValue() != trainee.getValue()) {
      return false;
    }
    if (!getName().equals(trainee.getName())) {
      return false;
    }
    return getSurname().equals(trainee.getSurname());
  }

  @Override
  public int hashCode() {
    int result = getName().hashCode();
    result = 31 * result + getSurname().hashCode();
    result = 31 * result + getValue();
    return result;
  }

  @Override
  public int compareTo(Trainee trainee) {
    return getName().compareTo(trainee.getName());
  }
}
