package com.stadnik.javasixthsemester.Trainee;

public enum TraineeErrorCodes {
  WRONG_NAME("Wrong name %s"),
  WRONG_SURNAME("Wrong surname %s"),
  WRONG_VALUE("Wrong value %s");

  private final String code;

  TraineeErrorCodes(String code) {
    this.code = code;
  }

  public String getMessage() {
    return code;
  }
}
