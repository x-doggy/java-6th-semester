package com.stadnik.javasixthsemester.Trainee;

public class TraineeException extends Exception {

  private static final long serialVersionUID = 3257152216018535794L;

public TraineeException() {
    super();
  }

  public TraineeException(String message) {
    super(message);
  }

  public TraineeException(Throwable cause) {
    super(cause);
  }

  public TraineeException(TraineeErrorCodes errCode) {
    super(errCode.getMessage());
  }

  public TraineeException(TraineeErrorCodes errCode, Throwable cause) {
    super(errCode.getMessage(), cause);
  }

  public TraineeException(TraineeErrorCodes errCode, String param) {
    super(String.format(errCode.getMessage(), param));
  }
}
