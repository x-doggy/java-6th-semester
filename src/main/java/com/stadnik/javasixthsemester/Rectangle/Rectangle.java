package com.stadnik.javasixthsemester.Rectangle;

public class Rectangle {
    private int top, left, right, bottom;

    public Rectangle(int top, int left, int bottom, int right) {
        setTop(top);
        setLeft(left);
        setBottom(bottom);
        setRight(right);
    }

    public Rectangle(int[] arr) {
        setTop(arr[0]);
        setLeft(arr[1]);
        setBottom(arr[2]);
        setRight(arr[3]);
    }

    public int getTop() {
        return top;
    }
    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }
    public void setLeft(int left) {
        this.left = left;
    }

    public int getBottom() {
        return bottom;
    }
    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getRight() {
        return right;
    }
    public void setRight(int right) {
        this.right = right;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle that = (Rectangle) o;

        if (getTop() != that.getTop()) return false;
        if (getLeft() != that.getLeft()) return false;
        if (getRight() != that.getRight()) return false;
        return getBottom() == that.getBottom();
    }

    @Override
    public int hashCode() {
        int result = getTop();
        result = 31 * result + getLeft();
        result = 31 * result + getRight();
        result = 31 * result + getBottom();
        return result;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "top=" + top +
                ", left=" + left +
                ", right=" + right +
                ", bottom=" + bottom +
                '}';
    }
}
