package com.stadnik.javasixthsemester.Group;

import com.stadnik.javasixthsemester.Trainee.Trainee;
import java.util.Arrays;
import java.util.Comparator;

public final class Group {

  private String groupName;
  private Trainee[] trainees;

  public Group(String groupName, Trainee[] trainees) throws GroupException {
    setGroupName(groupName);
    setTrainees(trainees);
  }

  public String getGroupName() {
    return groupName;
  }

  public Trainee[] getTrainees() {
    return trainees;
  }

  public void setGroupName(String groupName) throws GroupException {
    checkGroupName(groupName);
    this.groupName = groupName;
  }

  public void setTrainees(Trainee[] trainees) throws GroupException {
    checkTrainees(trainees);
    this.trainees = trainees;
  }

  private static void checkGroupName(String groupName) throws GroupException {
    if (groupName == null || groupName.equals("")) {
      throw new GroupException(GroupErrorCodes.INCORRECT_GROUP_NAME, groupName);
    }
  }

  private static void checkTrainees(Trainee[] trainees) throws GroupException {
    if (trainees == null) {
      throw new GroupException(GroupErrorCodes.INCORRECT_ARRAY_OF_TRAINEE, "");
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Group group = (Group) o;

    if (!getGroupName().equals(group.getGroupName())) {
      return false;
    }
    // Probably incorrect - comparing Object[] arrays with Arrays.equals
    return Arrays.equals(getTrainees(), group.getTrainees());
  }

  @Override
  public int hashCode() {
    int result = getGroupName().hashCode();
    result = 31 * result + Arrays.hashCode(getTrainees());
    return result;
  }

  @Override
  public String toString() {
    return "Group{"
            + "groupName='" + groupName + '\''
            + ", trainees=" + Arrays.toString(trainees)
            + '}';
  }

  public static Comparator<Group> NAME_COMPARATOR = Comparator.comparing(Group::getGroupName);
  public static Comparator<Trainee[]> TRAINEES_COMPARATOR = Comparator.comparing(Arrays::toString);

}
