package com.stadnik.javasixthsemester.Group;

public enum GroupErrorCodes {
  INCORRECT_GROUP_NAME("Wrong group name %s"),
  INCORRECT_ARRAY_OF_TRAINEE("Array of trainee is null or an empty array %s");

  private final String code;

  GroupErrorCodes(String code) {
    this.code = code;
  }

  public String getMessage() {
    return code;
  }
}
