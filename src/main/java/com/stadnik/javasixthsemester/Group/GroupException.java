package com.stadnik.javasixthsemester.Group;

public class GroupException extends Exception {

  private static final long serialVersionUID = -7074134848727181085L;

public GroupException() {
    super();
  }

  public GroupException(String message) {
    super(message);
  }

  public GroupException(Throwable cause) {
    super(cause);
  }

  public GroupException(GroupErrorCodes errCode) {
    super(errCode.getMessage());
  }

  public GroupException(GroupErrorCodes errCode, Throwable cause) {
    super(errCode.getMessage(), cause);
  }

  public GroupException(GroupErrorCodes errCode, String param) {
    super(String.format(errCode.getMessage(), param));
  }
}
