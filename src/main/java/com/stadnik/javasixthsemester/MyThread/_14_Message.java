package com.stadnik.javasixthsemester.MyThread;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Message {
    private final String emailAddress;
    private final String sender;
    private final String subject;
    private final String body;

    Message(String emailAddress, String sender, String subject, String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    @Override
    public String toString() {
        return "From: " + sender + "; To: " + emailAddress + "; Subject: " + subject + "; Body: " + body + ";\n";
    }
}

class Transport {
    public static void send(Message msg) throws IOException {
        try (FileWriter fw = new FileWriter("output.txt", true)) {
            fw.write(msg.toString());
        }
    }
}

public class _14_Message {
    public static void main(String args[]) throws FileNotFoundException {

        final File emails = new File("e-mails.txt");
        if (!emails.exists()) {
            System.err.println("File \"e-mails.txt\" not found!");
            return;
        }

        BufferedReader br = new BufferedReader(new FileReader(emails));
        ExecutorService es = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 10; i++)
            es.submit(() -> {
                String email;
                try {
                    email = br.readLine();
                    if (email != null)
                        Transport.send(new Message(email, "vstadnikv",
                                "Important news!",
                                "Stay tuned for Snoop Dogg\'s new album \'Neva Left\' comin\' to May 19!"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        es.shutdown();
        try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

}
