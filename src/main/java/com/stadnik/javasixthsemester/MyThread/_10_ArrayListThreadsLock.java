package com.stadnik.javasixthsemester.MyThread;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

class ProducerLock implements Runnable {
    private final ArrayList<Integer> list;
    private final ReentrantLock lock;
    final Thread t;

    ProducerLock(ArrayList<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
        this.t = new MyThread(this, "ProducerLock does add to list");
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            try {
                list.add(new Random().nextInt());
                System.out.println("[ProducerLock]");
            } finally {
                lock.unlock();
            }
        }
    }
}

class ConsumerLock implements Runnable {
    private final ArrayList<Integer> list;
    private final ReentrantLock lock;
    final Thread t;

    ConsumerLock(ArrayList<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
        this.t = new MyThread(this, "ConsumerLock does remove from list");
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            try {
                if (!list.isEmpty()) {
                    list.remove(new Random().nextInt(list.size()));
                    System.out.println("[ConsumerLock]");
                }
            } finally {
                lock.unlock();
            }
        }
    }
}

public class _10_ArrayListThreadsLock {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        ArrayList<Integer> list = new ArrayList<>();
        ProducerLock aat = new ProducerLock(list, lock);
        ConsumerLock rlt = new ConsumerLock(list, lock);

        try {
            aat.t.join();
            rlt.t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}