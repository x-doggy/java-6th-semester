package com.stadnik.javasixthsemester.MyThread;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MyHashMap<K, V> {
    /*
     * +---------------------+
     * | Паттерн "Декоратор" |
     * +---------------------+
     */

    private HashMap<K, V> map;
    private ReadWriteLock rwLock = new ReentrantReadWriteLock();

    MyHashMap() {
        this.map = new HashMap<>();
    }

    MyHashMap(HashMap<K, V> map) {
        this.map = map;
    }

    V get(K key) {
        rwLock.readLock().lock();
        try {
            return map.get(key);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    V put(K key, V value) {
        rwLock.writeLock().lock();
        try {
            return map.put(key, value);
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    Collection<V> values() {
        rwLock.readLock().lock();
        try {
            return map.values();
        } finally {
            rwLock.readLock().unlock();
        }
    }

    Set<K> keySet() {
        rwLock.readLock().lock();
        try {
            return map.keySet();
        } finally {
            rwLock.readLock().unlock();
        }
    }

    Object remove(K key) {
        rwLock.writeLock().lock();
        try {
            return map.remove(key);
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    boolean containsKey(Object key) {
        rwLock.readLock().lock();
        try {
            return map.containsKey(key);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    boolean containsValue(Object value) {
        rwLock.readLock().lock();
        try {
            return map.containsValue(value);
        } finally {
            rwLock.readLock().unlock();
        }
    }
}

public class _12_MyConcurrentHashMap {
    public static void main(String[] args) {
        MyHashMap<Integer, String> mhm = new MyHashMap<>();

        Runnable putTask = () -> {
            for (int i = 0; i < 20; i++) {
                mhm.put(i, String.valueOf(i));
                System.out.println(i + " added");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable removeTask = () -> {
            for (int i = 0; i < 20; i++) {
                mhm.remove(i);
                System.out.println(i + " removed");
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable containsTask = () -> {
            for (int i = 0; i < 20; i++) {
                System.out.println("Is " + i + " contains? -> " + mhm.containsKey(i));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t1 = new MyThread(putTask);
        Thread t2 = new MyThread(removeTask);
        Thread t3 = new MyThread(containsTask);

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            System.out.println("interrupted");
        }

    }
}
