package com.stadnik.javasixthsemester.MyThread;

import java.awt.*;
import java.util.Random;
import java.util.concurrent.Semaphore;

class MyBook {
    private int n;
    private Semaphore semReader;
    private Semaphore semWriter;

    MyBook() {
        this.semReader = new Semaphore(0);
        this.semWriter = new Semaphore(1);
        this.n = 0;
    }

    void get() {
        try {
            semReader.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Got: " + n);
        semWriter.release();
    }

    void put(int n) {
        try {
            semWriter.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.n = n;
        System.out.println("Put: " + n);
        semReader.release();
    }
}

class MyWriter implements Runnable {
    private MyBook q;
    Thread t;

    MyWriter(MyBook q) {
        this.q = q;
        this.t = new MyThread(this, "Writer");
    }

    public void run() {
        for (;;)
            q.put(new Random().nextInt(1000));
    }
}

class MyReader implements Runnable {
    private MyBook q;
    Thread t;

    MyReader(MyBook q) {
        this.q = q;
        this.t = new MyThread(this, "Reader");
    }

    public void run() {
        for (;;)
            q.get();
    }
}

public class _08_ReaderWriterSimple {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            MyBook q = new MyBook();
            new MyReader(q);
            new MyWriter(q);
            System.out.println("Press Ctrl+C to stop...");
        });
    }
}