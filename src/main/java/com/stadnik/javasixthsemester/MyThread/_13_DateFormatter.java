package com.stadnik.javasixthsemester.MyThread;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


final class Formatter {
    private static final ThreadLocal<SimpleDateFormat> thr = new ThreadLocal<>();

    public String format(Date date) {
        return getFormat().format(date);
    }

    private static DateFormat getFormat() {
        DateFormat format = thr.get();
        if (format == null) {
            format = new SimpleDateFormat("dd/MM/yyyy");
            thr.set((SimpleDateFormat) format);
        }
        return format;
    }
}

class Runner implements Runnable {
    private Formatter f;

    public Runner(Formatter f) {
        this.f = f;
    }

    @Override
    public void run() {
        System.out.println(f.format(new Date()));
    }
}

public class _13_DateFormatter {
    public static void main(String[] args) {
        Formatter f = new Formatter();
        ExecutorService es = Executors.newFixedThreadPool(5);
        es.submit(new Runner(f));
        es.submit(new Runner(f));
        es.submit(new Runner(f));
        es.submit(new Runner(f));
        es.submit(new Runner(f));
        es.shutdown();
    }
}
