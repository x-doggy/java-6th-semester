package com.stadnik.javasixthsemester.MyThread;

public class _03_WaitForThreeThreads {
    public static void main(String[] args) {
        MyThread mt1 = new MyThread("One");
        MyThread mt2 = new MyThread("Two");
        MyThread mt3 = new MyThread("Three");

        try {
            System.out.println("Waiting for 3 thread be ended");
            mt1.join();
            mt2.join();
            mt3.join();
        } catch (InterruptedException e) {
            System.err.println("Main thread was interrupted");
            e.printStackTrace();
        }

        if (!mt1.isAlive() && !mt2.isAlive() && !mt3.isAlive()) {
            System.out.println("Successfully wait!");
        }
    }
}
