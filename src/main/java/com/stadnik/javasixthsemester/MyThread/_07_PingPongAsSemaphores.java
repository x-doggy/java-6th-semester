package com.stadnik.javasixthsemester.MyThread;

import java.util.concurrent.Semaphore;

public class _07_PingPongAsSemaphores {

    public static void main(String[] args) {
        Semaphore semPing = new Semaphore(1);
        Semaphore semPong = new Semaphore(0);
        final int sleepTime = 500;

        Runnable ping = () -> {
            while (true) {
                try {
                    semPing.acquire();
                    System.out.println("Ping!");
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semPong.release();
                }
            }
        };

        Runnable pong = () -> {
            while (true) {
                try {
                    semPong.acquire();
                    System.out.println("Pong!");
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semPing.release();
                }
            }
        };

        new MyThread(ping, "Ping");
        new MyThread(pong, "Pong");

    }
}
