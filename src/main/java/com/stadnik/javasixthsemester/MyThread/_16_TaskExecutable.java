package com.stadnik.javasixthsemester.MyThread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.DefaultParser;

interface Executable {
    void execute();
}

class Task implements Executable {
    private int value;

    Task(int value) {
        this.value = value;
    }

    int getValue() {
        return value;
    }

    @Override
    public void execute() {
        System.out.println("[THREAD " + Thread.currentThread().getId() + "]: " + value);
    }
}

class TaskConsumer implements Runnable {
    private BlockingQueue<Task> queue;

    TaskConsumer(BlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Task task = queue.take();
                synchronized (System.out) {
                    System.out.println("[CONSUMER " + Thread.currentThread().getId() + "]: " + task.getValue());
                }
                if (task.getValue() == -1)
                    break;
                task.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

class TaskProducer implements Runnable {
    private BlockingQueue<Task> queue;
    private CountDownLatch cdl;

    TaskProducer(BlockingQueue<Task> queue, CountDownLatch cdl) {
        this.queue = queue;
        this.cdl = cdl;
    }

    @Override
    public void run() {
        int value;
        try {
            for (int i = 0; i < 5; i++) {
                value = (int) (Math.random() * 10);
                queue.add(new Task(value));
                synchronized (System.out) {
                    System.out.println("[PRODUCER " + Thread.currentThread().getId() + "]: " + value);
                }
            }
            cdl.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

public class _16_TaskExecutable {
    public static void main(String[] args) {
        final int PROCESSORS = Runtime.getRuntime().availableProcessors();
        final int DEVELOPERS;
        final int EXECUTORS;

        Options options = new Options()
                .addOption(new Option("d", "developers-number", true, "Number of developers"))
                .addOption(new Option("e", "executors-number", true, "Number of executors"));

        CommandLineParser cmdParser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = cmdParser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DEVELOPERS = (cmd != null && cmd.hasOption("d")) ? Integer.parseInt(cmd.getOptionValue("d")) : PROCESSORS;
        EXECUTORS  = (cmd != null && cmd.hasOption("e")) ? Integer.parseInt(cmd.getOptionValue("e")) : PROCESSORS;

        BlockingQueue<Executable> q = new LinkedBlockingQueue<>();

        Runnable developerTask = () -> {
            q.add( () -> {
                for (int i = 0; i < 1000; i++) {
                    for (int j = 0; j < 1000; j++) {
                        int dummy = i * j;
                    }
                }
            } );
        };

        Runnable executorTask = () -> {
            System.out.println("Executor has started his work...");
            try {
                q.take().execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Executor has stopped his work...");
        };

        for (int i = 0; i < DEVELOPERS; i++) {
            new MyThread(developerTask);
        }
        for (int i = 0; i < EXECUTORS; i++) {
            new MyThread(executorTask);
        }
    }
}
