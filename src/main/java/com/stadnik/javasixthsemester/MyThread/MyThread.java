package com.stadnik.javasixthsemester.MyThread;

public class MyThread extends Thread {
    public MyThread() {
        super();
        start();
    }
    public MyThread(Runnable r) {
        super(r);
        start();
    }
    public MyThread(String s) {
        super(s);
        start();
    }
    public MyThread(Runnable r, String s) {
        super(r, s);
        start();
    }
}

