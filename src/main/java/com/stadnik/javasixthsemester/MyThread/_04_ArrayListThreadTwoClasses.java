package com.stadnik.javasixthsemester.MyThread;

import java.util.ArrayList;
import java.util.Random;

class Producer implements Runnable {
    private final ArrayList<Integer> list;
    final Thread t;

    Producer(ArrayList<Integer> list) {
        this.list = list;
        this.t = new MyThread(this, "ProducerLock does add to list");
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++)
            synchronized (list) {
                list.add(new Random().nextInt());
                System.out.println("[ProducerLock]");
            }
    }
}

class Consumer implements Runnable {
    private final ArrayList<Integer> list;
    final Thread t;

    Consumer(ArrayList<Integer> list) {
        this.list = list;
        this.t = new MyThread(this, "ConsumerLock does remove from list");
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++)
            synchronized (list) {
                if (!list.isEmpty()) {
                    list.remove(new Random().nextInt(list.size()));
                    System.out.println("[ConsumerLock]");
                }
            }
    }
}

public class _04_ArrayListThreadTwoClasses {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        Producer aat = new Producer(list);
        Consumer rlt = new Consumer(list);

        try {
            aat.t.join();
            rlt.t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}