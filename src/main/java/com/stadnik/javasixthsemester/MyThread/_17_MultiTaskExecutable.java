package com.stadnik.javasixthsemester.MyThread;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class MultiTask {

    private List<Executable> list;

    MultiTask(List<Executable> list) {
        this.list = list;
    }

    List<Executable> getList() {
        return list;
    }

}

public class _17_MultiTaskExecutable {
    public static void main(String[] args) {
        final int PROCESSORS = Runtime.getRuntime().availableProcessors();
        final int DEVELOPERS;
        final int EXECUTORS;

        Options options = new Options()
                .addOption(new Option("d", "developers-number", true, "Number of developers"))
                .addOption(new Option("e", "executors-number", true, "Number of executors"));

        CommandLineParser cmdParser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = cmdParser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DEVELOPERS = (cmd != null && cmd.hasOption("d")) ? Integer.parseInt(cmd.getOptionValue("d")) : PROCESSORS;
        EXECUTORS  = (cmd != null && cmd.hasOption("e")) ? Integer.parseInt(cmd.getOptionValue("e")) : PROCESSORS;

        BlockingQueue<MultiTask> queue = new LinkedBlockingQueue<>();

        Runnable multiTaskExecutor = () -> {
            MultiTask mt;
            Task t;
            while (true) {
                try {
                    mt = queue.take();
                    if (!mt.getList().isEmpty()) {
                        t = (Task) mt.getList().remove(0);
                        if (t.getValue() == -1)
                            break;
                        t.execute();
                        queue.add(mt);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable multiTaskDeveloper = () -> {
            List<Executable> list = new ArrayList<>();
            list.add(new Task((int) (Math.random() * 10)));
            queue.add(new MultiTask(list));
        };

        Runnable multiTaskMonitor = () -> {
            List<Executable> list = new ArrayList<>();
            list.add(new Task(-1));
            for (int i = 0; i < EXECUTORS; i++)
                queue.add(new MultiTask(list));
        };

        for (int i = 0; i < DEVELOPERS; i++) {
            new MyThread(multiTaskExecutor);
        }
        for (int i = 0; i < EXECUTORS; i++) {
            new MyThread(multiTaskDeveloper);
        }
        new MyThread(multiTaskMonitor);
    }
}
