package com.stadnik.javasixthsemester.MyThread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class PingPongLock {
    private Lock lock;
    private Condition pinged;
    private Condition ponged;

    public PingPongLock() {
        lock = new ReentrantLock();
        pinged = lock.newCondition();
        ponged = lock.newCondition();
    }

    public void doPing() {
        try {
            lock.lock();
            System.out.println("Ping!");
            pinged.signalAll();
            ponged.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void doPong() {
        try {
            lock.lock();
            System.out.println("Pong!");
            ponged.signalAll();
            pinged.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}

public class _11_PingPongAsLocksAndConditions {
    public static void main(String[] args) {
        final PingPongLock pingPong = new PingPongLock();
        final int sleepTime = 500;

        Runnable ping = () -> {
            while (true) {
                try {
                    pingPong.doPing();
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable pong = () -> {
            while (true) {
                try {
                    pingPong.doPong();
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new MyThread(ping, "Ping");
        new MyThread(pong, "Pong");
    }
}
