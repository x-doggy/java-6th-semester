package com.stadnik.javasixthsemester.MyThread;

import java.util.ArrayList;
import java.util.Random;

class SynchronizedResource {
    ArrayList<Integer> al;

    SynchronizedResource(ArrayList<Integer> al) {
        this.al = al;
    }

    synchronized void remove(int index) {
        System.out.println("[Sync resource]\t To remove: " + index);
        al.remove(index);
    }

    synchronized void add(int num) {
        System.out.println("[Sync resource]\t To add: " + num);
        al.add(num);
    }
}

class ProducerConsumer implements Runnable {
    private SynchronizedResource sr;
    private final boolean mode;
    final Thread t;

    ProducerConsumer(SynchronizedResource sr, final boolean mode) {
        this.sr = sr;
        this.mode = mode;
        this.t = new MyThread(this, "ProducerAndConsumer");
    }

    @Override
    public void run() {
        if (mode)
            add();
        else
            remove();
    }

    private void add() {
        for (int i=0; i < 10000; i++) {
            synchronized (this) {
                sr.add(new Random().nextInt(10000));
            }
        }
    }

    private void remove() {
        for (int i=0; i < 10000; i++) {
            synchronized (this) {
                try {
                    if (!sr.al.isEmpty())
                        sr.remove(new Random().nextInt(sr.al.size()));
                } catch (IndexOutOfBoundsException e) {
                    System.err.println("Выход за пределы массива");
                }
            }
        }
    }
}


public class _05_ArrayListThreadOneClass {
    public static void main(String[] args) {
        SynchronizedResource sr = new SynchronizedResource( new ArrayList<>() );
        ProducerConsumer producer = new ProducerConsumer(sr, true);
        ProducerConsumer consumer = new ProducerConsumer(sr, false);

        try {
            producer.t.join();
            consumer.t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}