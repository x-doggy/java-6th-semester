package com.stadnik.javasixthsemester.MyThread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

class ProducerAndConsumer implements Runnable {
    private final List<Integer> sr;
    private Random rnd = new Random();

    ProducerAndConsumer(List<Integer> sr) {
        this.sr = sr;
        new MyThread(this, "ProducerAndConsumer");
    }

    @Override
    public void run() {
        add();
        remove();
    }

    private void add() {
        for (int i = 0; i < 10000; i++) {
            final int value = rnd.nextInt(10000);
            System.out.println("To add: " + value);
            sr.add(value);
        }
    }

    private void remove() {
        for (int i = 0; i < 10000; i++) {
            try {
                final int value = rnd.nextInt(10000);
                System.out.println("To remove: " + value);
                sr.remove(value);
            } catch (IndexOutOfBoundsException e) {
                System.err.println("Выход за пределы массива");
            }
        }
    }
}

public class _06_SyncList {
    public static void main(String[] args) {
        List<Integer> syncList = Collections.synchronizedList( new ArrayList<Integer>() );
        new ProducerAndConsumer(syncList);
        new ProducerAndConsumer(syncList);
    }
}
