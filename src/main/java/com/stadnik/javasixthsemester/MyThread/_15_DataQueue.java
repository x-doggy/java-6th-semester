package com.stadnik.javasixthsemester.MyThread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.cli.*;

class Data {
    private int[] array;

    Data(int[] array) {
        this.array = array;
    }

    int[] get() {
        return this.array;
    }
}

public class _15_DataQueue {
    public static void main(String[] args) {
        final int PROCESSORS = Runtime.getRuntime().availableProcessors();
        final int CONSUMERS;
        final int PRODUCERS;
        final int DATA_LENGTH;

        Options options = new Options()
                .addOption(new Option("c", "consumers-number", true, "Number of consumers"))
                .addOption(new Option("p", "producers-number", true, "Number of producers"))
                .addOption(new Option("l", "data-length", true, "Length of data"));

        CommandLineParser cmdParser = new DefaultParser();

        CommandLine cmd = null;
        try {
            cmd = cmdParser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        CONSUMERS   = (cmd != null && cmd.hasOption("c")) ? Integer.parseInt(cmd.getOptionValue("c")) : PROCESSORS;
        PRODUCERS   = (cmd != null && cmd.hasOption("p")) ? Integer.parseInt(cmd.getOptionValue("p")) : PROCESSORS;
        DATA_LENGTH = (cmd != null && cmd.hasOption("l")) ? Integer.parseInt(cmd.getOptionValue("l")) : PROCESSORS;

        BlockingQueue<Data> q = new LinkedBlockingQueue<>();

        Runnable DataConsumer = () -> {
            int[] data = new int[DATA_LENGTH];
            for (int i = 0; i < DATA_LENGTH; i++)
                data[i] = (int) (Math.random() * 50);
            q.add(new Data(data));
        };

        Runnable DataProducer = () -> {
            int[] data = null;
            try {
                data = q.take().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (data == null) return;

            synchronized (System.out) {
                for (int i : data) {
                    System.out.print(i + "\t");
                }
                System.out.println("");
            }
        };

        for (int i = 0; i < CONSUMERS; i++) {
            new MyThread(DataConsumer);
        }
        for (int i = 0; i < PRODUCERS; i++) {
            new MyThread(DataProducer);
        }
    }
}
