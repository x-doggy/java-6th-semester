package com.stadnik.javasixthsemester.MyThread;

public class _02_WaitForAnotherThread {
    public static void main(String[] args) {
        MyThread mt = new MyThread(() -> System.out.println("New thread started!"));

        try {
            System.out.println("Waiting for thread be ended");
            mt.join();
        } catch (InterruptedException e) {
            System.err.println("Main thread was interrupted");
            e.printStackTrace();
        }

        if (!mt.isAlive()) {
            System.out.println("Successfully wait!");
        }
    }
}
