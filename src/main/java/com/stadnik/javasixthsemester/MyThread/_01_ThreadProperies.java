package com.stadnik.javasixthsemester.MyThread;

public class _01_ThreadProperies {
    public static void main(String[] args) {
        Thread t = Thread.currentThread();
        System.out.println("+++ CURRENT THREAD INFORMATION: +++");
        System.out.println("ID =                   " + t.getId());
        System.out.println("Name =                 " + t.getName());
        System.out.println("Context class loader = " + t.getContextClassLoader().toString());
        System.out.println("Priority =             " + t.getPriority());
        System.out.println("State =                " + t.getState().toString());
        System.out.println("Thread group =         " + t.getThreadGroup().toString());
    }
}
