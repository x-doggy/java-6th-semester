package com.stadnik.javasixthsemester.Color;

public enum Color {
  RED(255, 0, 0),
  GREEN(0, 255, 0),
  BLUE(0, 0, 255),
  BLACK(0, 0, 0),
  WHITE(255, 255, 255);

  private final int r;
  private final int g;
  private final int b;

  private static boolean isIncorrect(int d) {
    return d < 0 || d > 255;
  }

  Color(int r, int g, int b) {
    this.r = isIncorrect(r) ? 0 : r;
    this.g = isIncorrect(g) ? 0 : g;
    this.b = isIncorrect(b) ? 0 : b;
  }
}
