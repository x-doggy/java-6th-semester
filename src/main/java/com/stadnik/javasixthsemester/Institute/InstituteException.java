package com.stadnik.javasixthsemester.Institute;

public class InstituteException extends Exception {

  private static final long serialVersionUID = 8254826756543477241L;

public InstituteException() {
    super();
  }

  public InstituteException(String message) {
    super(message);
  }

  public InstituteException(Throwable cause) {
    super(cause);
  }

  public InstituteException(InstituteErrorCodes errCode) {
    super(errCode.getMessage());
  }

  public InstituteException(InstituteErrorCodes errCode, Throwable cause) {
    super(errCode.getMessage(), cause);
  }

  public InstituteException(InstituteErrorCodes errCode, String param) {
    super(String.format(errCode.getMessage(), param));
  }
}
