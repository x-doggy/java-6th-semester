package com.stadnik.javasixthsemester.Institute;

public enum InstituteErrorCodes {
  INCORRECT_INSTITUTE_NAME("Wrong institute name %s"),
  INCORRECT_INSTITUTE_CITY("Wrong institute city %s");

  private final String code;

  InstituteErrorCodes(String code) {
    this.code = code;
  }

  public String getMessage() {
    return code;
  }
}
