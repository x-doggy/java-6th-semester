package com.stadnik.javasixthsemester.Institute;

public final class Institute {

  private String name;
  private String city;

  public Institute(String name, String city) throws InstituteException {
    setName(name);
    setCity(city);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) throws InstituteException {
    checkName(name);
    this.name = name;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) throws InstituteException {
    checkCity(city);
    this.city = city;
  }

  private static void checkName(String name) throws InstituteException {
    if (name == null || name.equals("")) {
      throw new InstituteException(InstituteErrorCodes.INCORRECT_INSTITUTE_NAME, name);
    }
  }

  public static void checkCity(String city) throws InstituteException {
    if (city == null || city.equals("")) {
      throw new InstituteException(InstituteErrorCodes.INCORRECT_INSTITUTE_CITY, city);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Institute institute = (Institute) o;

    if (!getName().equals(institute.getName())) {
      return false;
    }
    return getCity().equals(institute.getCity());
  }

  @Override
  public int hashCode() {
    int result = getName().hashCode();
    result = 31 * result + getCity().hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Institute{"
            + "name='" + name + '\''
            + ", city='" + city + '\''
            + '}';
  }

}
